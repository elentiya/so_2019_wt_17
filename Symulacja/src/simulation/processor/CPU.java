package simulation.processor;

import simulation.taskGenerator.Task;

public class CPU {
    private Task currentTask;

    public CPU(){
        currentTask = null;
    }

    public Task getExecutedTask() {
        return currentTask;
    }

    public void setExecutedTask(Task currentTask) {
        this.currentTask = currentTask;
    }

    public boolean isBusy(){
        return currentTask != null || currentTask.getRequiredTime() > 0;
    }

}
