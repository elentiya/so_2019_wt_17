package simulation.processor;

import java.util.ArrayList;

public class CPUGroup {

    private ArrayList<CPU> cpuList;
    private int size;

    public CPUGroup(int size){
        this.size = size;
        cpuList = new ArrayList<>();
        create();
    }

    private void create(){
        for(int i=0; i<size; i++){
            cpuList.add(new CPU());
        }
    }

    public ArrayList<CPU> getCpuList() {
        return cpuList;
    }
    
    public int getSize() {
        return size;
    }

    public ArrayList<CPU> getFreeCPUs() {
        ArrayList<CPU> freeCPUs = new ArrayList<>();
        for(int i=0;i<size;i++){
            if(!(cpuList.get(i).isBusy())){
                freeCPUs.add(cpuList.get(i));
            }
        }
        return freeCPUs;
    }

    public ArrayList<CPU> getBusyCPUs() {
        ArrayList<CPU> busyCPUs = new ArrayList<>();
        for(int i=0;i<size;i++){
            if((cpuList.get(i).isBusy())){
                busyCPUs.add(cpuList.get(i));
            }
        }
        return busyCPUs;
    }

    public int getFreeCPUsAmmount(){
        return getFreeCPUs().size();
    }

}
