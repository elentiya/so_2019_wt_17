package simulation.processor;

import simulation.manager.ProcessProvider;
import simulation.manager.Tick;
import simulation.taskGenerator.Task;

import java.security.Provider;
import java.util.ArrayList;

//import simulation.manager.TaskManager;

public class Service implements Tick {
    private ProcessProvider provider;
    private CPUGroup cpuGr;
    private ArrayList<Task> tasksToProcede;

    Service(int noOfCPU, ProcessProvider provider) {
        this.provider = provider;
        cpuGr = new CPUGroup(noOfCPU);
    }

    public ArrayList<Task> getTasksToProcede() {
        return tasksToProcede;
    }

    public void setTasksToProcede(ArrayList<Task> tasksToProcede) {
        this.tasksToProcede = tasksToProcede;
    }

    @Override
    public void tick(long currentTick) {
        tasksToProcede = provider.getTaskList(cpuGr.getFreeCPUsAmmount());
        allocateTasks(tasksToProcede);
        lowerTaskTimeExecution();
        checkIfProcceded();
    }

    private void allocateTasks(ArrayList<Task> tasks) {
        if(cpuGr.getFreeCPUsAmmount() == 0){
            return;
        }
        if(tasks.size() == 0){
            return;
        }
        for(int i=0; i<tasks.size(); i++){
            cpuGr.getFreeCPUs().get(i).setExecutedTask(tasks.get(i));
        }
    }

    private void lowerTaskTimeExecution(){
        for(CPU cpu: cpuGr.getBusyCPUs()){
            cpu.getExecutedTask().setRequiredTime(cpu.getExecutedTask().getRequiredTime() - 1);
        }
    }

    private void checkIfProcceded(){
        for(CPU cpu: cpuGr.getBusyCPUs()){
            if(cpu.getExecutedTask().getRequiredTime() <= 0){
                cpu.setExecutedTask(null);
            }
        }
    }
}

/* Zastanowic sie jak pogrupowac te klasy; kto robi maina ktora odpali cala aplikacja */