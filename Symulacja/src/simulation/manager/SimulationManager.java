package simulation.manager;
import simulation.dataStructures.LinkedList;
import simulation.presenter.Presenter;
import simulation.processor.TaskManager;
import simulation.schedulingAlgorithm.FCFS;
import simulation.schedulingAlgorithm.SchedulingAlgorithm;
import simulation.taskGenerator.Task;
import simulation.taskGenerator.TaskGenerator;

import java.util.ArrayList;

public class SimulationManager implements Tick{
    
    private int cpuNumber = 5;
    private int processNumber = 20;
    private SchedulingAlgorithm algorithm = new FCFS();
    
    private Clock clock;
    private Thread thread;
    private LinkedList<Task> queue = new LinkedList<>();
    private ProcessProvider provider = new ProcessProvider(queue, algorithm);
    
    private TaskManager taskManager = new TaskManager(cpuNumber,provider); // Not implemented
    private Presenter presenter = new Presenter();
    private TaskGenerator taskGenerator = new TaskGenerator(clock,queue,processNumber);
    
    public SimulationManager (SchedulingAlgorithm algorithm){
        // Get Input
        this.algorithm = algorithm;
        clock = new Clock();
        thread = new Thread(clock);
        clock.addObserver(this);
        thread.start();
    }
    
    public void startSimulation(SchedulingAlgorithm algorithm){
        this.algorithm = algorithm;
        cleanUp();
        thread.start();
    }
    
    private void cleanUp(){
        clock.removeObserver(taskManager);
        thread = new Thread(clock);
        queue = new LinkedList<>();
        provider = new ProcessProvider(queue,algorithm);
        taskManager = new TaskManager(cpuNumber,provider);
    }
    
    private void printResults(){
        double average = provider.getSum()/provider.getDoneCount();
        
        //presenter.SaveData(algorithm.getName(),average);
    }
    
    public void setAlgorithm(SchedulingAlgorithm algorithm){
        provider = new ProcessProvider(queue, algorithm);
    }
    
    @Override
    public void tick (long currentTick) {
        //System.out.println(currentTick);
        if(provider.getDoneCount() >= processNumber){
            clock.stop();
            printResults();
        }
    }
}