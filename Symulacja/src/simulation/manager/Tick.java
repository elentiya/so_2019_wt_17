package simulation.manager;

public interface Tick {
    void tick(long currentTick);
}
