package simulation.taskGenerator;


import simulation.dataStructures.LinkedList;
import simulation.manager.Clock;
import simulation.manager.Tick;

import java.util.ArrayList;
import java.util.Random;

public class TaskGenerator implements Tick {

    private ArrayList<Task> tasks;
    private LinkedList<Task> list;

    public TaskGenerator(Clock clock, LinkedList<Task> list, int n) {
         clock.addObserver(this);
         tasks = generate(20,4, n);
         this.list = list;
    }
 
    public ArrayList<Task> generate(int timeLimit, int limit, int n) {
        Random rand = new Random();
        ArrayList<Task> list = new ArrayList<>();

        for (int i = 0 ; i < n ; i++) {
            list.add(new Task(rand.nextInt(timeLimit) + 1, rand.nextInt(limit)));
        }
 
        return list;
    }

    @Override
    public void tick(long currentTick) {
        for (Task task : tasks) {
            if (task.getTime() == currentTick) {
                list.add(task);
            }
        }
    }
}