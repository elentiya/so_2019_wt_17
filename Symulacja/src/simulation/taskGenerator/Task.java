package simulation.taskGenerator;

public class Task {
	 
    private int requiredTime;
    private int time;
 
    public Task(int requiredTime, int time) {
        this.requiredTime = requiredTime;
        this.time  =time;
    }
 
    public int getRequiredTime() {
        return requiredTime;
    }
 
    public void setRequiredTime(int requiredTime) {
        this.requiredTime = requiredTime;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Task{" +
                "requiredTime=" + requiredTime +
                ", time=" + time +
                '}';
    }
}