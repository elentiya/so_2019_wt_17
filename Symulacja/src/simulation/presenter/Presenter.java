package simulation.presenter;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class Presenter {
    private int liczba_procesorow;
    private int liczba_procesow;

    public int getLiczba_procesow() {
        return liczba_procesow;
    }

    public void setLiczba_procesow(int liczba_procesow) {
        this.liczba_procesow = liczba_procesow;
    }



    private HashMap<String, Double> result;


    public Presenter() {
        result = new HashMap<>();
        Writer output = null;
        try {
            output = new BufferedWriter(new FileWriter("text.txt", false));
            output.append("Symulacja algorytmów planowania dostępu do procesoraów: \n");
            output.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void saveData(String name, double time) {
        this.result.put(name, time);
    }


    public void print() {
        for (Map.Entry<String, Double> it : result.entrySet()) {
            System.out.println("Sredni czas dla algotytmu: " +
                    it.getKey() + " jest równy   " + it.getValue());
        }
    }

    public void start() throws IOException {
        System.out.println("Ile będzie procesów?");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        liczba_procesorow = Integer.parseInt(reader.readLine());
    }

    public int getLiczba_procesorow() {
        return liczba_procesorow;
    }

    public void setLiczba_procesorow(int liczba_procesorow) {
        this.liczba_procesorow = liczba_procesorow;
    }


    public void saveDataInFile() throws FileNotFoundException {
        Writer output = null;
        try {
            for (Map.Entry<String, Double> it : result.entrySet()) {
                output = new BufferedWriter(new FileWriter("text.txt", true));
                output.append("Sredni czas dla algorytmu " + it.getKey() + ": " + it.getValue() + "\n");
                output.close();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

}