package simulation.schedulingAlgorithm;

import simulation.dataStructures.LinkedList;
import simulation.taskGenerator.Task;

import java.util.ArrayList;

public interface SchedulingAlgorithm {
    
    ArrayList<Task> findNext(LinkedList<Task> list, int n);
    String getName();
    
}
