package simulation.schedulingAlgorithm;

import simulation.dataStructures.LinkedList;
import simulation.taskGenerator.Task;

import java.util.ArrayList;

public class SJF implements SchedulingAlgorithm {
    
    @Override
    public ArrayList<Task> findNext (LinkedList<Task> list, int n) {
       ArrayList<Task> temp = new ArrayList<>();
       if(list == null || n<=0) return temp;
       
       if(n >= list.size()){
           for(int i = 0; i < list.size(); i++){
                temp.add(findMin(list));
           }
       }
       else{
           for(int i = 0; i < n; i++){
               temp.add(findMin(list));
           }
       }
       return temp;
    }
    
    private Task findMin(LinkedList<Task> list){
        Task min = list.get(0);
        for(Task t: list){
            if(t.getPriority() < min.getPriority()) min = t;
        }
        list.remove(min);
        return min;
    }
    
    @Override
    public String getName () {
        return "SJF";
    }
}
