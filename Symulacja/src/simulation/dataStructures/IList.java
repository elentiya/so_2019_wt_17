package simulation.dataStructures;

public interface IList<E> {
    
    void clear();
    boolean isEmpty();
    boolean add(E element);
    boolean add(int position, E element);
    boolean contains(E element);
    int size();
    int indexOf(E element);
    E get(int position);
    E set(int position, E element);
    E remove(int n);
    boolean remove(E element);

}
