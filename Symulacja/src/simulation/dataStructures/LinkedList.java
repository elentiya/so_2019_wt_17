package simulation.dataStructures;

import java.util.Iterator;

public class LinkedList<E> implements IList<E>, Iterable<E> {
    
    private Element head = null;
    
    @Override
    public void clear () {
        head = null;
    }
    
    @Override
    public boolean isEmpty () {
        return head == null;
    }
    
    private Element getElement(int position) throws IndexOutOfBoundsException,NullPointerException{
        if(position<0) throw new IndexOutOfBoundsException();
        Element e = head;
        while(position>0 && e!=null){
            position--;
            e = e.getNext();
        }
        if(e == null) throw new NullPointerException();
        return e;
    }
    
    @Override
    public boolean add (E element) {
        int position = 0;
        if(head == null){
            head = new Element(element);
            return true;
        }
        Element actualElement = head;
        while(actualElement.getNext()!= null){
            actualElement = actualElement.getNext();
        }
        actualElement.setNext(new Element(element));
        return true;
    }
    
    @Override
    public boolean add (int position, E element) throws IndexOutOfBoundsException{
        if(position<0) throw new IndexOutOfBoundsException();
        Element newElement = new Element(element);
        if(position == 0){
            newElement.setNext(head);
            head = newElement;
            return true;
        }
        Element e = getElement(position-1);
        newElement.setNext(e.getNext());
        e.setNext(newElement);
        return true;
    }
    
    @Override
    public boolean contains (E element) {
        return indexOf(element)>=0;
    }
    
    @Override
    public int size () {
        int position = 0;
        Element e = head;
        while(e!=null){
            position++;
            e=e.getNext();
        }
        return position;
    }
    
    @Override
    public int indexOf (E element) {
        int position = 0;
        Iterator<E> iterator = iterator();
        while(iterator.hasNext()){
            if(iterator.next().equals(element)) return position;
            position++;
        }
        return  -1;
    }
    
    @Override
    public E get (int position) {
        Element e = getElement(position);
        return e.getValue();
    }
    
    @Override
    public E set (int position, E element) {
        Element e = getElement(position);
        E temp = e.getValue();
        e.setValue(element);
        return temp;
    }
    
    @Override
    public E remove (int position) {
        if(position<0 || head.getValue()==null) throw new IndexOutOfBoundsException();
        if(position ==0){
            E temp = head.getValue();
            head = head.getNext();
            return temp;
        }
        Element e = getElement(position-1);
        if(e.getNext()==null)
            throw new IndexOutOfBoundsException();
        E retValue = e.getNext().getValue();
        e.setNext(e.getNext().getNext());
        return retValue;
    }
    
    @Override
    public boolean remove (E element) {
        if(head == null) return false;
        if(head.getValue().equals(element)){
            head = head.getNext();
            return true;
        }
        Element e = head;
        while(e.getNext() != null && !e.getValue().equals(element)) e = e.getNext();
        if(e.getNext()==null) return false;
        e.setNext(e.getNext().getNext());
        return true;
    }
    
    @Override
    public Iterator<E> iterator () {
        return new InnerIterator();
    }
    
    private class Element{
        private Element next = null;
        private E value;
        
        public Element (E value) {
            this.value = value;
        }
        
        public Element getNext () {
            return next;
        }
        
        public void setNext (Element next) {
            this.next = next;
        }
        
        public E getValue () {
            return value;
        }
        
        public void setValue (E value) {
            this.value = value;
        }
    }
    
    private class InnerIterator implements Iterator<E>{
        
        Element e;
        public InnerIterator () {
            e = head;
        }
        
        @Override
        public boolean hasNext () {
            return e!=null;
        }
        
        @Override
        public E next () {
            E returnValue = e.getValue();
            e = e.getNext();
            return returnValue;
        }
        
        @Override
        public void remove () {
            throw new UnsupportedOperationException();
        }
    }

}
