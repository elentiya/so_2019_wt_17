package simulation.dataStructures;

import java.util.EmptyStackException;
import java.util.Iterator;

public class LinkedQueue<E> implements Queue<E>,Iterable<E> {
    private Element<E> head = null;
    
    @Override
    public boolean isEmpty () {
        return head == null;
    }
    
    @Override
    public boolean clear () {
        if(head == null) return false;
        head = null;
        return true;
    }
    
    @Override
    public E dequeue () throws EmptyStackException {
        if(head == null) throw new EmptyStackException();
        Element<E> element = head;
        head = element.getNextElement();
        return element.getData();
    }
    
    @Override
    public boolean enqueue (E element) {
        if(isEmpty()){
            head = new Element<>(element);
            return true;
        }
        
        Element e = head;
        while(e.getNextElement() != null){
            e = e.getNextElement();
        }
        
        e.setNextElement(new Element(element));
        return true;
    }
    
    @Override
    public int size () {
        int size = 0;
        Element e = head;
        while(e != null){
            size++;
            e = e.getNextElement();
        }
        return size;
    }
    
    @Override
    public E peekNext () {
        Element<E> e = head;
        if(e.getNextElement() == null) throw new EmptyStackException();
        e = e.getNextElement();
        return  e.getData();
    }
    
    @Override
    public E peek (int position) {
        return null;
    }
    
    @Override
    public Iterator<E> iterator () {
        return new InnerIterator();
    }
    
    private class InnerIterator implements Iterator<E>{
        
        private Element<E> current = head;
        
        @Override
        public boolean hasNext () {
            return current != null;
        }
        
        @Override
        public E next () {
            E temp = current.getData();
            current = current.getNextElement();
            return temp;
        }
        
        @Override
        public void remove () {
            throw new UnsupportedOperationException();
        }
    }
    
    private  class Element<E>{
        private E data;
        private Element<E> nextElement = null;
        
        public Element(E data){
            this.data = data;
        }
        
        public E getData () {
            return data;
        }
        
        public void setData (E data) {
            this.data = data;
        }
        
        public Element getNextElement () {
            return nextElement;
        }
        
        public void setNextElement (Element nextElement) {
            this.nextElement = nextElement;
        }
    }
    
}
